<OpenSpanDesignDocument Version="8.0.2000.3" Serializer="2.0" Culture="en-US">
  <ComponentInfo>
    <Type Value="OpenSpan.Automation.Automator" />
    <Assembly Value="OpenSpan.Automation" />
    <AssemblyReferences>
      <Assembly Value="System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="OpenSpan, Version=8.0.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Automation, Version=8.0.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Controls, Version=8.0.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Runtime.Core, Version=8.0.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    </AssemblyReferences>
    <DynamicAssemblyReferences />
    <FileReferences />
    <BuildReferences />
  </ComponentInfo>
  <Component Version="1.0">
    <OpenSpan.Automation.Automator Name="Automation1" Id="Automator-8D68CF5CFEC7C1E">
      <AutomationDocument>
        <Name Value="" />
        <Size Value="5000, 5000" />
        <Objects>
          <ConnectionBlock>
            <DisplayName Value="RuntimeLoader.AllProjectsStarted" />
            <ConnectableUniqueId Value="Automator-8D68CF5CFEC7C1E\ConnectableEvent-8D68CF5D504D98A" />
            <PartID Value="1" />
            <Left Value="100" />
            <Top Value="40" />
            <Collapsed Value="True" />
            <WillExecute Value="True" />
            <InstanceName Value="OpenSpan.Runtime.RuntimeLoader" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Show" />
            <ConnectableUniqueId Value="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5DAAD041B" />
            <PartID Value="2" />
            <Left Value="1300" />
            <Top Value="60" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="messageDialog1" />
            <Fittings>
              <Result Collapsed="True" ActualText="Result" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Format" />
            <ConnectableUniqueId Value="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" />
            <PartID Value="4" />
            <Left Value="1060" />
            <Top Value="40" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="stringUtils1" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="GetDeploymentVersion" />
            <ConnectableUniqueId Value="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F7E993DC" />
            <PartID Value="5" />
            <Left Value="440" />
            <Top Value="40" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="OpenSpan.Runtime.RuntimeHost" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="GetProjectVersion" />
            <ConnectableUniqueId Value="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F8EC4B9D" />
            <PartID Value="6" />
            <Left Value="760" />
            <Top Value="40" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="OpenSpan.Runtime.RuntimeHost" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="TerminateRuntime" />
            <ConnectableUniqueId Value="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5FF52F701" />
            <PartID Value="7" />
            <Left Value="1520" />
            <Top Value="60" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="OpenSpan.Runtime.RuntimeHost" />
            <OverriddenIds />
          </ConnectionBlock>
        </Objects>
        <Links>
          <Link PartID="10" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="1" PortName="Raised" PortType="Event" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableEvent-8D68CF5D504D98A" MemberComponentId="Automator-8D68CF5CFEC7C1E\EMPTY" />
            <To PartID="5" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F7E993DC" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F7E993DC" />
            <LinkPoints>
              <Point value="368, 69" />
              <Point value="378, 69" />
              <Point value="407, 69" />
              <Point value="407, 69" />
              <Point value="435, 69" />
              <Point value="445, 69" />
            </LinkPoints>
          </Link>
          <Link PartID="11" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="5" PortName="Complete" PortType="Event" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F7E993DC" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F7E993DC" />
            <To PartID="6" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F8EC4B9D" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F8EC4B9D" />
            <LinkPoints>
              <Point value="694, 69" />
              <Point value="704, 69" />
              <Point value="729, 69" />
              <Point value="729, 69" />
              <Point value="755, 69" />
              <Point value="765, 69" />
            </LinkPoints>
          </Link>
          <Link PartID="12" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="6" PortName="Complete" PortType="Event" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F8EC4B9D" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F8EC4B9D" />
            <To PartID="4" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" />
            <LinkPoints>
              <Point value="1014, 69" />
              <Point value="1024, 69" />
              <Point value="1024, 69" />
              <Point value="1024, 69" />
              <Point value="1055, 69" />
              <Point value="1065, 69" />
            </LinkPoints>
          </Link>
          <Link PartID="13" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="6" PortName="Result" PortType="Property" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F8EC4B9D" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F8EC4B9D" />
            <To PartID="4" PortName="list0" PortType="Property" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" />
            <LinkPoints>
              <Point value="1014, 86" />
              <Point value="1024, 86" />
              <Point value="1028, 86" />
              <Point value="1028, 103" />
              <Point value="1055, 103" />
              <Point value="1065, 103" />
            </LinkPoints>
          </Link>
          <Link PartID="14" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="5" PortName="Result" PortType="Property" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F7E993DC" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F7E993DC" />
            <To PartID="4" PortName="list1" PortType="Property" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" />
            <LinkPoints>
              <Point value="694, 86" />
              <Point value="704, 86" />
              <Point value="708, 86" />
              <Point value="708, 120" />
              <Point value="1055, 120" />
              <Point value="1065, 120" />
            </LinkPoints>
          </Link>
          <Link PartID="15" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="4" PortName="Complete" PortType="Event" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" />
            <To PartID="2" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5DAAD041B" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5DAAD041B" />
            <LinkPoints>
              <Point value="1271, 69" />
              <Point value="1281, 69" />
              <Point value="1284, 69" />
              <Point value="1284, 89" />
              <Point value="1295, 89" />
              <Point value="1305, 89" />
            </LinkPoints>
          </Link>
          <Link PartID="16" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="4" PortName="Result" PortType="Property" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5F2621DF7" />
            <To PartID="2" PortName="message" PortType="Property" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5DAAD041B" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5DAAD041B" />
            <LinkPoints>
              <Point value="1271, 154" />
              <Point value="1281, 154" />
              <Point value="1284, 154" />
              <Point value="1284, 106" />
              <Point value="1295, 106" />
              <Point value="1305, 106" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="17" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="2" ParentMemberName="Result" DecisionValue="Yes" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5DAAD041B" />
            <To PartID="7" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5FF52F701" MemberComponentId="Automator-8D68CF5CFEC7C1E\ConnectableMethod-8D68CF5FF52F701" />
            <LinkPoints>
              <Point value="1451, 137" />
              <Point value="1461, 137" />
              <Point value="1460, 137" />
              <Point value="1460, 137" />
              <Point value="1468, 137" />
              <Point value="1468, 89" />
              <Point value="1515, 89" />
              <Point value="1525, 89" />
            </LinkPoints>
          </DecisionEventLink>
        </Links>
        <Comments />
        <SubGraphs />
      </AutomationDocument>
    </OpenSpan.Automation.Automator>
    <OpenSpan.Automation.ConnectableEvent Name="connectableEvent1" Id="ConnectableEvent-8D68CF5D504D98A">
      <ComponentName Value="OpenSpan.Runtime.RuntimeLoader" />
      <DisplayName Value="RuntimeLoader.AllProjectsStarted" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Runtime.RuntimeLoader" />
      <InstanceUniqueId Value="EMPTY" />
      <MemberDetails Value=".AllProjectsStarted Event" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="AllProjectsStarted" />
            <MemberType Value="Event" />
            <TypeName Value="System.EventHandler" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableEvent>
    <OpenSpan.Controls.MessageDialog Name="messageDialog1" Id="MessageDialog-8D68CF5D9E0B634">
      <Buttons Value="YesNo" />
      <Caption Value="Information" />
      <Scope Value="Local" Extended="True" />
    </OpenSpan.Controls.MessageDialog>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod1" Id="ConnectableMethod-8D68CF5DAAD041B">
      <ComponentName Value="messageDialog1" />
      <DisplayName Value="Show" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Controls.MessageDialog" />
      <InstanceUniqueId Value="Automator-8D68CF5CFEC7C1E\MessageDialog-8D68CF5D9E0B634" />
      <MemberDetails Value=".Show() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="False" type="System.Windows.Forms.DialogResult" aliasName="Result" shouldSerialize="False" visibility="DefaultOn" source="" blockTypeName="" />
        </Items>
      </Content>
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="Show" />
            <MemberType Value="Method" />
            <TypeAssemblyName Value="System.Windows.Forms" />
            <TypeName Value="System.Windows.Forms.DialogResult" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Windows.Forms.DialogResult" />
                <Content Name="ParameterPrototype">
                  <Items>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="False" />
                      <CanWrite Value="True" />
                      <DefaultSet Value="False" />
                      <DefaultValue Value="message" />
                      <ParamName Value="message" />
                      <Position Value="0" />
                      <TypeName Value="System.String" />
                    </OpenSpan.Automation.ParameterPrototype>
                  </Items>
                </Content>
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Controls.StringUtils Name="stringUtils1" Id="StringUtils-8D68CF5ED81B7EE">
      <Scope Value="Local" Extended="True" />
    </OpenSpan.Controls.StringUtils>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod2" Id="ConnectableMethod-8D68CF5F2621DF7">
      <ComponentName Value="stringUtils1" />
      <DisplayName Value="Format" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Controls.StringUtils" />
      <InstanceUniqueId Value="Automator-8D68CF5CFEC7C1E\StringUtils-8D68CF5ED81B7EE" />
      <MemberDetails Value=".Format() Method" />
      <ParamsLength Value="3" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="False" type="System.String" aliasName="Result" shouldSerialize="False" visibility="DefaultOn" source="" blockTypeName="" />
        </Items>
      </Content>
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="Format" />
            <MemberType Value="Method" />
            <TypeName Value="System.String" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.String" />
                <Content Name="ParameterPrototype">
                  <Items>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="False" />
                      <CanWrite Value="True" />
                      <DefaultSet Value="True" />
                      <DefaultValue Value="GetProjectVersion: {0}// GetDeploymentVersion: {1}//&#xD;&#xA;&#xD;&#xA;Exit?" />
                      <ParamName Value="formatString" />
                      <Position Value="0" />
                      <TypeName Value="System.String" />
                    </OpenSpan.Automation.ParameterPrototype>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="False" />
                      <CanWrite Value="True" />
                      <DefaultSet Value="False" />
                      <ParamName Value="list" />
                      <Position Value="1" />
                      <TypeName Value="System.String[]" />
                    </OpenSpan.Automation.ParameterPrototype>
                  </Items>
                </Content>
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod3" Id="ConnectableMethod-8D68CF5F7E993DC">
      <ComponentName Value="OpenSpan.Runtime.RuntimeHost" />
      <DisplayName Value="GetDeploymentVersion" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Runtime.RuntimeHost" />
      <InstanceUniqueId Value="EMPTY" />
      <MemberDetails Value=".GetDeploymentVersion() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="False" type="System.Version" aliasName="Result" shouldSerialize="False" visibility="DefaultOn" source="" blockTypeName="" />
        </Items>
      </Content>
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="GetDeploymentVersion" />
            <MemberType Value="Method" />
            <TypeName Value="System.Version" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Version" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod4" Id="ConnectableMethod-8D68CF5F8EC4B9D">
      <ComponentName Value="OpenSpan.Runtime.RuntimeHost" />
      <DisplayName Value="GetProjectVersion" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Runtime.RuntimeHost" />
      <InstanceUniqueId Value="EMPTY" />
      <MemberDetails Value=".GetProjectVersion() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="False" type="System.Version" aliasName="Result" shouldSerialize="False" visibility="DefaultOn" source="" blockTypeName="" />
        </Items>
      </Content>
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="GetProjectVersion" />
            <MemberType Value="Method" />
            <TypeName Value="System.Version" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Version" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod5" Id="ConnectableMethod-8D68CF5FF52F701">
      <ComponentName Value="OpenSpan.Runtime.RuntimeHost" />
      <DisplayName Value="TerminateRuntime" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Runtime.RuntimeHost" />
      <InstanceUniqueId Value="EMPTY" />
      <MemberDetails Value=".TerminateRuntime() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="TerminateRuntime" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
  </Component>
</OpenSpanDesignDocument>